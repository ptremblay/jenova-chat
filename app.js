/**
 * Created by Patrick on 2015-03-04.
 */
'use strict';

var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.set('port', (process.env.PORT || 5000));

app.get('/', function(req,res){
    res.sendFile(__dirname +'/public/index.html');
});

io.on('connection',function(socket){
    console.log('Client connected.');
    socket.on('messages', function(data){
        console.log('messages event!' + data);
        socket.broadcast.emit("messages",data);
        socket.emit("messages", data);
    });

    socket.on('join', function(data){
        socket.nickname = data;
        socket.broadcast.emit("join",data);
        socket.emit("join", data);
    });

});

app.use(express.static('public'));

http.listen(app.get('port'), function(){
    console.log('simple-chat listening on port %d',app.get('port'));
});

